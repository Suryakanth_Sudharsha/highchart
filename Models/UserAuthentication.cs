﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiProject.Models
{
    public class UserAuthentication : IdentityDbContext
    {
        public UserAuthentication(DbContextOptions options): base(options)
        {
            //this.Database.CommandTimeout = 180;
        }

        public DbSet<AppUser> ApplicationUsers  { get; set; }
    }
}
