﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebApiProject.Models
{
    public class AppConfig
    {
        public string JWT_Secret { get; set; }
        public string Client_URL { get; set; }
    }
}
