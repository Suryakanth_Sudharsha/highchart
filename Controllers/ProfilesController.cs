﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using WebApiProject.Models;

namespace WebApiProject.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProfilesController : ControllerBase
    {
        
            private UserManager<AppUser> _userManager;
            public ProfilesController(UserManager<AppUser> userManager)
            {
                _userManager = userManager;
            }

            [HttpGet]
            [Authorize]
            //GET : /api/UserProfile
            public async Task<Object> GetUserProfile()
            {
                string claimId = User.Claims.First(c => c.Type == "UserID").Value;
                var user = await _userManager.FindByIdAsync(claimId);
                return new
                {
                    user.FullName,
                    user.Email,
                    user.UserName
                };
            }
        
    }
}